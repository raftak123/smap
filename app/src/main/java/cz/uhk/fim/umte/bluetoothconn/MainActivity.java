package cz.uhk.fim.umte.bluetoothconn;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    public void Ucitel(View view) {
        Intent intent = new Intent(MainActivity.this,Ucitel.class);
        finish();
        startActivity(intent);
    }

    public void Zak(View view) {
        Intent intent = new Intent(MainActivity.this,Zak.class);
        finish();
        startActivity(intent);
    }
}

