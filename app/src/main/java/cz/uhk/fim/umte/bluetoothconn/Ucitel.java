package cz.uhk.fim.umte.bluetoothconn;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Ucitel extends AppCompatActivity {

    private Handler handler;
    private ConnectToDevice connectToDevice;
    private ReceiveImage receiveImage;
    private int i=0;
    private Boolean scan=false;

    //Konstanty pro handler
    private interface MessageConstants { int MESSAGE_WRITE = 1;int MESSAGE_Failed = -1;}

    private BluetoothAdapter myBluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
    private ListView listViewPaired;
    private static final UUID MY_UUID=UUID.fromString("8ce255c0-223a-11e0-ac64-0803450c9a66");
    private TextView connInfo,pairedText;
    private String connectedDevice="";
    private Button reloadPaired,scanBtn;
    private ProgressBar progressBar;

    private ArrayList<BluetoothDevice> btDevArray=new ArrayList<>();
    private ArrayAdapter<String> adapterPaired;
    private ArrayList<String> listPaired = new ArrayList<>();

    private GridView gvGallery;
    private GridBaseAdapter gridBaseAdapter;
    private ArrayList<ImageModel> imageModelArrayList;
    private List<String> myImageNameList = new ArrayList<>();
    private List<Bitmap> myImageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ucitel);

        gvGallery = findViewById(R.id.gv);
        listViewPaired=findViewById(R.id.AlreadyPaired);
        connInfo=findViewById(R.id.ConnInfo);
        pairedText=findViewById(R.id.pairedTextview);
        reloadPaired=findViewById(R.id.buttReloadPaired);
        scanBtn=findViewById(R.id.ButtnScan);
        progressBar=findViewById(R.id.progressFind);

        if(!myBluetoothAdapter.isEnabled()) {
            myBluetoothAdapter.enable();
        }

        showPaired();
        implementListeners();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(ConnectionInfo, filter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(ConnectionInfo);
        super.onPause();
    }

    @Override
    protected void onResume() {
        //Filter na zachycení odpojení.
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(ConnectionInfo, filter);
        super.onResume();
    }

    //list obrázků a textů v gridView
    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();
        for(int i = 0; i < myImageList.size(); i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setName(myImageNameList.get(i));
            imageModel.setImage_drawable(myImageList.get(i));
            list.add(imageModel);
        }
        return list;
    }

    //Tlačítko scan
    public void ScanDevices(View view) {
        if(scanBtn.getText().equals("Start scan")) {
            if(btDevArray.size()>0){
            scanBtn.setText("Stop scan");
            myImageNameList.clear();
            myImageList.clear();
            gvGallery.setAdapter(null);

            scan = true;
            i = 0;

            connInfo.setText("Stav: probíhá skenování dustopných zařízení.");
            MakeInvis();

            connectedDevice = btDevArray.get(0).getName();
            connectToDevice = new ConnectToDevice(btDevArray.get(0));
            connectToDevice.start();
            }else
                Toast.makeText(this,"Nejprve spárujte zařízení.",Toast.LENGTH_LONG).show();
        }
        else {
            scan=false;
            scanBtn.setText("Stopping scan......");
        }
    }

    //Při použítí scanu, skok na další zařížení
    public void NextScan()
    {
        i++;
        connectedDevice=btDevArray.get(i).getName();
        connectToDevice = new ConnectToDevice(btDevArray.get(i));
        connectToDevice.start();
    }

    private void MakeInvis()
    {
        listViewPaired.setAdapter(null);
        pairedText.setVisibility(View.GONE);
        reloadPaired.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }
    private void MakeVisib()
    {
        scanBtn.setVisibility(View.VISIBLE);
        scanBtn.setText("Start scan");
        progressBar.setVisibility(View.GONE);
        listViewPaired.setAdapter(adapterPaired);
        pairedText.setVisibility(View.VISIBLE);
        reloadPaired.setVisibility(View.VISIBLE);
    }

    public void implementListeners()
    {

        //List spárovaných zařízení, umožňující kliknout na samostatné zařízení.
        listViewPaired.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                scan=false;
                scanBtn.setVisibility(View.INVISIBLE);
                connectedDevice=btDevArray.get(position).getName();
                connInfo.setText("Stav: připojování k zařízení: "+connectedDevice);
                myImageNameList.clear();
                myImageList.clear();
                gvGallery.setAdapter(null);
                i=0;

                MakeInvis();

                connectToDevice = new ConnectToDevice(btDevArray.get(position));
                connectToDevice.start();
            }
        });

        //Handler pro úspěšné/neúspěčné přečtení
        handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if(msg.what==MessageConstants.MESSAGE_WRITE){
                    byte[] readBuff=(byte[]) msg.obj;
                    Bitmap bitmap = BitmapFactory.decodeByteArray(readBuff,0,msg.arg1);

                    Date d=new Date(new Date().getTime());
                    String date=new SimpleDateFormat("HH:mm:ss").format(d);

                    myImageNameList.add(connectedDevice+"-"+date);
                    myImageList.add(bitmap);

                    imageModelArrayList = populateList();
                    gridBaseAdapter = new GridBaseAdapter(getApplicationContext(),imageModelArrayList);
                    gvGallery.setAdapter(gridBaseAdapter);

                    if(scan) {
                        if (i + 1 != btDevArray.size()) {
                            NextScan();
                        } else {
                            MakeVisib();
                        }
                    }
                }
                if(msg.what==MessageConstants.MESSAGE_Failed){
                    connInfo.setText("Stav: při připojení "+connectedDevice+" nastala chyba.");
                    connectToDevice.cancel();
                    if(scan)
                    {
                        if(i+1!=btDevArray.size())
                        {
                            NextScan();
                        }else
                            {
                                MakeVisib();
                            }
                    }else MakeVisib();
                }
                return true;
            }
        });
    }


    //Handler pro info ohledně připojení
    public final BroadcastReceiver ConnectionInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                System.out.println("Disconected");
                if(!scan)
                {
                    MakeVisib();
                }

            }
        }
    };

    //Zobrazení spárovaných zařízení do listview
    public void showPaired()
    {
        listPaired.clear();
        adapterPaired=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listPaired);
        listViewPaired.setAdapter(adapterPaired);

        Set<BluetoothDevice> pairedDevices = myBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                btDevArray.add(device);
                listPaired.add(device.getName()+" MAC:"+device.getAddress());
            }
        }
        adapterPaired.notifyDataSetChanged();

    }

    //Vlákno pro připojení k zařízení
    private class ConnectToDevice extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectToDevice(final BluetoothDevice device) {
            BluetoothSocket tmp = null;
            mmDevice = device;
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                handler.obtainMessage(-1,connectedDevice).sendToTarget();
                Log.e("Error","Chyba při navázání spojení.");
            }
            mmSocket = tmp;
        }

        public void run() {
            myBluetoothAdapter.cancelDiscovery();

            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                handler.obtainMessage(-1,connectedDevice).sendToTarget();
                Log.e("Error","Chyba při připojení k zařízení.");
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e("Error","Chyba při uzavření socketu.");
                }
                return;
            }

            //Připojení bylo úspěšné
            manageMyConnectedSocket(mmSocket);
        }

        private void manageMyConnectedSocket(BluetoothSocket mmSocket) {
            connInfo.setText("Stav: "+connectedDevice+" připojeno");
            receiveImage =new ReceiveImage(mmSocket,mmDevice);
            receiveImage.start();
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                System.out.println("Proc: Could not close the client socket");
            }
        }
    }

    private class ReceiveImage extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ReceiveImage(BluetoothSocket socket,BluetoothDevice bluetoothDevice) {
            mmSocket = socket;
            InputStream tmpIn = null;
            BluetoothDevice deviceConnected=bluetoothDevice;

            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                handler.obtainMessage(-1,connectedDevice);
                Log.e( "Error","Chyba při vytváření inputstreamu");
            }
            mmInStream = tmpIn;
        }

        public void run() {
            byte[] buffer = null;
            int numberOfBytes =0;
            int index =0;
            boolean flag= true;

            mmBuffer = new byte[1024];
            int numBytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                if(flag)
                {
                    try {
                        byte[] temp = new byte[mmInStream.available()];
                        if(mmInStream.read(temp)>0)
                        {
                            try {
                                numberOfBytes=Integer.parseInt(new String(temp,"UTF-8"));
                                buffer= new byte[numberOfBytes];
                                flag=false;
                            }catch (Exception e)
                            {
                                handler.obtainMessage(-1,connectedDevice).sendToTarget();
                                e.printStackTrace();
                            }
                        }
                    }catch (IOException e)
                    {
                        handler.obtainMessage(-1,connectedDevice).sendToTarget();
                        e.printStackTrace();
                    }
                }else
                {
                    try {
                        byte[] data = new byte [mmInStream.available()];
                        int numbers=mmInStream.read(data);

                        System.arraycopy(data,0,buffer,index,numbers);
                        index=index+numbers;

                        if(index ==numberOfBytes){
                            handler.obtainMessage(MessageConstants.MESSAGE_WRITE,numberOfBytes,-1,buffer).sendToTarget();
                            flag=true;
                        }
                    }catch (IOException e)
                    {
                        handler.obtainMessage(-1,connectedDevice).sendToTarget();
                    }
                }
            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                System.out.println("Proc: Could not close the connect socket");
            }
        }
    }


    public void ReloadPaired(View view) {
        showPaired();
    }
}

