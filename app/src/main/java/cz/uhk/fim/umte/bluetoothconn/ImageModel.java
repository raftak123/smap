package cz.uhk.fim.umte.bluetoothconn;

import android.graphics.Bitmap;

/**
 * Created by Parsania Hardik on 03-Jan-17.
 */
public class ImageModel {

    private String name;
    private Bitmap image_drawable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage_drawable() {
        return image_drawable;
    }

    public void setImage_drawable(Bitmap image_drawable) {
        this.image_drawable = image_drawable;
    }

}