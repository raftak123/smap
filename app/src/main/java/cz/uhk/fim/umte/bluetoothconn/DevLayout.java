package cz.uhk.fim.umte.bluetoothconn;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DevLayout {


    public TextView AddDevName (Context context){
    LinearLayout.LayoutParams DevNameLayParam = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    DevNameLayParam.setMargins(0,20,0,0);

    TextView devName = new TextView(context);
    devName.setLayoutParams(DevNameLayParam);
    devName.setTextSize((float) (devName.getTextSize()*0.6));
    devName.setTypeface(Typeface.DEFAULT_BOLD);

    return devName;
        }

    public ImageView AddDevImg (Context context){
        LinearLayout.LayoutParams DevImgLayParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        DevImgLayParam.gravity= Gravity.CENTER;

        ImageView devImg = new ImageView(context);
        devImg.setLayoutParams(DevImgLayParam);
        devImg.setBackgroundResource(R.mipmap.ic_launcher);
        devImg.getLayoutParams().height = (int) context.getResources().getDimension(R.dimen.screen_height);
        devImg.getLayoutParams().width = (int) context.getResources().getDimension(R.dimen.screen_width);
        return devImg;
    }

}


