package cz.uhk.fim.umte.bluetoothconn;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;

public class Zak extends AppCompatActivity {

    private FileTransfer fileTransfer;
    private PrepareConnection prepareConnection;
    private Boolean left=false;
    private static final UUID MY_UUID=UUID.fromString("8ce255c0-223a-11e0-ac64-0803450c9a66");
    private BluetoothAdapter bluetoothAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zak);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        prepareConnection = new PrepareConnection();
        prepareConnection.start();


        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(ConnectionInfo, filter);
    }

    @Override
    protected void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(ConnectionInfo, filter);
        super.onResume();
    }

    @Override
    protected void onUserLeaveHint() {
        left=true;
        super.onUserLeaveHint();
    }

    //Handler pro info ohledně připojení
    private final BroadcastReceiver ConnectionInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                prepareConnection = new PrepareConnection();
                prepareConnection.start();
            }
        }
    };

    private class PrepareConnection extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public PrepareConnection() {
            BluetoothServerSocket tmp = null;
            try {
                tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord("BTTransFLOYOP", MY_UUID);
            } catch (IOException e) {
                Log.e("Error","Proc: Nebylo možné navázat spojení.");
            }
            mmServerSocket = tmp;
        }

        public void run() {
            bluetoothAdapter.cancelDiscovery();
            BluetoothSocket socket = null;
            //Zařízení neustále poslouchá, dokud něco nepříjde, poté socket!=null
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    System.out.println("Proc: Nastal problém s přijímáním.");
                    break;
                }

                if (socket != null) {
                    //Připojení bylo navázáno
                    manageMyConnectedSocket(socket);
                    try {
                        mmServerSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

        private void manageMyConnectedSocket(BluetoothSocket socket) {

            fileTransfer=new FileTransfer(socket);
            fileTransfer.start();
            SendImage();
        }

        // Uzavření server socketu pro připojení
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                System.out.println("Proc: Nepodařilo se uzavřít socket.");
            }
        }
    }

    private class FileTransfer extends Thread {
        private final BluetoothSocket mmSocket;
        private final OutputStream mmOutStream;

        public FileTransfer(BluetoothSocket socket) {
            mmSocket = socket;
            OutputStream tmpOut = null;

            //Vytvoření outputstreamu
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                System.out.println("Proc: Chyba při vytváření outputstreamu.");
            }
            mmOutStream = tmpOut;
        }

        // Zápis do outputstreamu
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
                mmOutStream.flush();

            } catch (IOException e) {
                Log.e("error","Proc: Chyba při posílání dat.");
            }
        }

        // Uzavření socketu po odeslání.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                System.out.println("Proc: Chyba při uzavření socketu.");
            }
        }
    }

    private Bitmap getDeviceScreen()
    {
        try {
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            if(left)
                bitmap.eraseColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            return bitmap;

        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    private void SendImage()
    {
        Bitmap bitmap= getDeviceScreen();
        ByteArrayOutputStream stream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,30,stream);
        byte[] imageBytes=stream.toByteArray();

        int subArraySize=400;

        fileTransfer.write(String.valueOf(imageBytes.length).getBytes());

        for(int i=0;i<imageBytes.length;i+=subArraySize)
        {
            byte[] tempArray;
            tempArray= Arrays.copyOfRange(imageBytes,i,Math.min(imageBytes.length,i+subArraySize));
            fileTransfer.write(tempArray);
        }

        fileTransfer.cancel();
        prepareConnection.cancel();
    }
}
